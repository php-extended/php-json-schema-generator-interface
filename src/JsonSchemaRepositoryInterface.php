<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use Psr\Http\Message\UriInterface;
use RuntimeException;
use Stringable;

/**
 * JsonSchemaRepositoryInterface interface file.
 * 
 * This represents a way to get json schema data from a given schema id.
 * 
 * @author Anastaszor
 */
interface JsonSchemaRepositoryInterface extends Stringable
{
	
	/**
	 * Gets the schema from the schema id.
	 * 
	 * @param UriInterface $id
	 * @return JsonSchemaInterface
	 * @throws InvalidArgumentException if the uri is not complete
	 * @throws RuntimeException if getting the data is impossible
	 */
	public function findJsonSchema(UriInterface $id) : JsonSchemaInterface;
	
}
