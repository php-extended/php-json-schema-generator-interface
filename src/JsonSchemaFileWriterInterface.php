<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use RuntimeException;
use Stringable;

/**
 * JsonSchemaFileWriterInterface interface file.
 * 
 * This is to create a writing strategy and executes it from a file collection.
 * 
 * @author Anastaszor
 */
interface JsonSchemaFileWriterInterface extends Stringable
{
	
	/**
	 * Writes all the files that were generated to the right place where they
	 * should belong.
	 * 
	 * @param JsonSchemaFileCollectionInterface $collection
	 * @return integer the number of files written
	 * @throws RuntimeException if a write fails
	 */
	public function write(JsonSchemaFileCollectionInterface $collection) : int;
	
}
