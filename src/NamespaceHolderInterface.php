<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * NamespaceHolderInterface interface file.
 * 
 * This represents all the namespaces that are needed to generate a group of
 * related classes (interface, class, test) for a given entity.
 * 
 * @author Anastaszor
 */
interface NamespaceHolderInterface extends Stringable
{
	
	/**
	 * Gets the namespace for the interfaces.
	 * 
	 * @return string
	 */
	public function getInterfaceNamespace() : string;
	
	/**
	 * Gets the namespace for the base classes.
	 * 
	 * @return string
	 */
	public function getClassNamespace() : string;
	
	/**
	 * Gets the namespace for the tests classes.
	 * 
	 * @return string
	 */
	public function getTestNamespace() : string;
	
}
