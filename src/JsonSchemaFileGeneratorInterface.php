<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * JsonSchemaFileGeneratorInterface class file.
 * 
 * This transforms the json schema into a collection of interfaces and classes
 * and other runnable class elements, depending of the version and language
 * choosen for the rendering.
 * 
 * @author Anastaszor
 */
interface JsonSchemaFileGeneratorInterface extends Stringable
{
	
	/**
	 * Transforms a json schema structure into a runnable collection of files
	 * that have to be written to the right directory data structure to be able
	 * to be autoloaded and work together.
	 * 
	 * @param JsonSchemaInterface $schema
	 * @return JsonSchemaFileCollectionInterface
	 */
	public function generate(JsonSchemaInterface $schema) : JsonSchemaFileCollectionInterface;
	
}
