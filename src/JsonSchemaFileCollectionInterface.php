<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * JsonSchemaFileCollectionInterface interface file.
 * 
 * This gathers all the files that have to be generated from a single json
 * schema file.
 * 
 * @author Anastaszor
 */
interface JsonSchemaFileCollectionInterface extends Stringable
{
	
	/**
	 * Gets the contents of the interface files, indexed by their fqcn interface
	 * names.
	 * 
	 * @return array<class-string, string>
	 */
	public function getInterfaceFiles() : array;
	
	/**
	 * Gets the contents of the class files, indexed by their fqcn class names.
	 * 
	 * @return array<class-string, string>
	 */
	public function getClassFiles() : array;
	
	/**
	 * Gets the contents of the test files, indexed by their fqcn class names.
	 * 
	 * @return array<class-string, string>
	 */
	public function getTestFiles() : array;
	
	/**
	 * Merges this collection with another collection, returns a new object
	 * collection if other is not null with both files from each collection.
	 * 
	 * @param ?JsonSchemaFileCollectionInterface $other
	 * @return JsonSchemaFileCollectionInterface
	 */
	public function mergeWith(?JsonSchemaFileCollectionInterface $other = null) : JsonSchemaFileCollectionInterface;
	
}
